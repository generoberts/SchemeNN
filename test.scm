(define de 10)
(define w '(1 2 3))
(define cal ; should give (60 20 10)
  (lambda (de ws dept collector)
    name: 'cal
    (cond
     ((= dept (length ws)) collector)
     ((= dept 0) (cal de ws (+ dept 1) (cons de collector)))
     (else
      (cal de ws (+ dept 1) (cons
                             (* (list-ref ws dept)
                                (car collector))
                             collector))))))
(define (main)
  (cal de w 0 '()))
