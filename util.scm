;;; util.scm -- Utilities for reading csv file and saving model in sexp.

;; Commentary:
;; This file contain all the function to processing data before/after we put
;; the data as input in  a model.
;; TODO: Maybe we should expand this file for more ease of use.

;; Code:

(define-library (schemeNN util)
  (export read-csv)
  (import (scheme base)
          (scheme file))
  (begin

    ;; Remove all TARGET from the list LST.
    ;; Returns a list that doesn't contain TARGET.
    ;; Usage: (remove #\a '(#\a #\b #\c #\a #\a)) ; => (#\b #\c)
    (define (remove target lst)
      (cond
       ((null? lst) '())
       ((eq? target (car lst)) (remove target (cdr lst)))
       (else
        (cons (car lst) (remove target (cdr lst))))))

    ;; Turns a list of chars to a list of string of one character.
    ;; Returns a list of strings.
    (define (chars->strings lst)
      (map (lambda (ch) (string ch)) lst))

    ;; FILE is a string indicates filename.
    ;; Returns the list of lists, which is a content of the csv file in string form.
    ;; We can use `map` to turn those result into number, character, etc. easily.
    (define (read-csv file)
      
      ;; Read csv file line-by-line.
      ;; Returns the list of read lines.
      (define (csv->list-of-list file)
        (call-with-input-file file
          (lambda (l)
            (let f ((result '())
                    (line (read-line l)))
              (cond
               ((eof-object? line)
                (reverse result))
               (else
                ;; use list line because we want to make the result of the form
                ;; lists-in-list, and the inner lists will act as a line separater.
                (f (cons (list line) result)
                   (read-line l))))))))
      
      (let ((the-list (csv->list-of-list file))) ; it's of the form (("1,2") ("3,4"))
        (map (lambda (line) (chars->strings ; turns a list of chars to a list of string
                             (remove #\, ; removes commas from a list of chars
                                     (string->list ; turns a string to a list of chars 
                                      (car line))))) ; take the first element of the line,
                                        ; which has only 1 element that is,
                                        ; for example ("1,2") for the first line
             the-list)))))

;;; util.scm ends here.
