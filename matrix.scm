;;; matrix.scm --- matrix definition and operations.

;; Commentary:
;; All about matrix goes in this file. Making them, oprerating on them, and specific
;; creating kind of matrix are defined in this file.

;; Code:

(define-library (schemeNN matrix)
  (export make-matrix matrix?
          matrix-content set-matrix!
          set-row-num! set-col-num!
          matrix-mul matrix-apply
          element-wise-matrix-operation
          hadamard-product matrix-minus
          identity-matrix random-matrix)
  (import (scheme base)
          (schemeNN random))
  (begin

    ;; Matrix in its core is a list of lists, along with additional information such as
    ;; its number of rows, and its number of columns.
    (define-record-type <matrix>
      (matrix lst row col)
      matrix?
      (lst matrix-content set-matrix!)
      (row matrix-row set-row-num!)
      (col matrix-col set-col-num!))

    ;; Making matrix this way so we don't have to specify matrix dimension manually.
    ;; Returns a matrix whose content is LST.
    ;; The vector [a b c] is cearted using (make-matrix '((a b c)))
    ;; The transpose vector
    ;; [a
    ;;  b
    ;; c]
    ;; is created using (make-matrix '((a) (b) (c)))
    (define (make-matrix lst)
      (cond
       ((matrix? lst)
        lst)
       ((list? (car lst))
        (matrix lst
                (length lst)
                (length (car lst))))
       (else
        (error "Not a valid matrix."))))

    ;; Matrix multiplication. The algorithm is taken from
    ;; https://rosettacode.org/wiki/Matrix_multiplication
    (define (matrix-mul m1 m2)
      (cond
       ((not (matrix? m1))
        (error "M1 isn't a matrix."))
       ((not (matrix? m2))
        (error "M2 isn't a matrix."))
       ((not (= (matrix-col m1) (matrix-row m2)))
        (error "Invalid matrix dimension."))
       (else
        (map
         (lambda (row)
           (apply map
                  (lambda column
                    (apply + (map * row column)))
                  (matrix-content m2)))
         (matrix-content m1)))))

    ;; Applies a procedure FUNC to each element of matrix MTRX.
    ;; Returns a matrix.
    (define (matrix-apply func mtrx)
      (cond
       ((not (procedure? func))
        (error "FUNC isn't a procedure."))
       ((not (matrix? mtrx))
        (error "MTRX isn't a matrix."))
       (else
        (make-matrix (map (lambda (r) (map (lambda (element) (func element))
                                           r))
                          (matrix-content mtrx))))))

    ;; Applying operation, which must take 2 arguments to the matrix 1 and 2.
    ;; OP is a procedure that must take at 2 arguments.
    ;; M1 is the first matrix.
    ;; M2 is a second matrix.
    ;; M1 and M2 mustc have the same dimension.
    ;; Returns a new matrix.
    (define (element-wise-matrix-operation op m1 m2)
      (if (and (= (matrix-row m1) (matrix-row m2))
               (= (matrix-col m1) (matrix-col m2)))
          (make-matrix (map (lambda (first-matrix second-matrix)
                              (map (lambda (first-list second-list)
                                     (map (lambda (first-list-element second-list-element)
                                            (op first-list-element second-list-element))
                                          first-list second-list))
                                   first-matrix second-matrix))
                            m1 m2))
          (error "Invalide matrix dimensions.")))

    ;; Element-Wise product.
    ;; M1 and M2 are matrixes.
    ;; Returns a matrix.
    ;; TODO: Test.
    (define (hadamard-product m1 m2)
      (element-wise-matrix-operation * m1 m2))

    ;; Minus matrixes element-wise.
    ;; M1 and M2 are matrix.
    ;; Returns a matrix.
    (define (matrix-minus m1 m2)
      (element-wise-matrix-operation - m1 m2))

    ;; Create identity matrix with a given size.
    ;; Returns ann identity matrix.
    ;; TODO: Test. Following the convension shouldn't change how this function behave.
    (define (identity-matrix size)
      ;; Retruns a list of lists representing identity matrix.
      (define (identity-matrix-builder size)
        (define (make-list-helper size index) ; all but the index is 0, the index is 1
          (if (= 0 size)
              '()
              (cons (if (= index (- size 1))
                        1
                        0)
                    (make-list-helper (- size 1) index))))
        (define (make-list size index)
          (reverse (make-list-helper size index)))
        (define (make-zeros size) ; a list of 0s
          (if (= 0 size)
              '()
              (cons 0 (make-zeros (- size 1)))))
        (do ((first-list (make-zeros size))
             (i 0 (+ i 1)))
            ((= i size) first-list)
          (set! (list-ref first-list i)
                (make-list size i))))
      (if (number? size)
          (make-matrix (identity-matrix-builder size))
          (error "Not a valid matrix size.")))

    ;; Create a matrix of dimension SIZE x SIZE  whose element is
    ;; an inexact number between 0 and 1.
    ;; Returns a matrix.
    ;; TODO: Test. Following convension shouldn't change how this function behave.
    (define (random-matrix size)
      (define (random-matrix-builder size)
        (define (random-row s)
          (if (= 0 s)
              '()
              (cons (rrandom) (random-row (- s 1)))))
        (do ((result '() (cons (random-row size) result))
             (i 0 (+ i 1)))
            ((= i size) result)))
      (if (number? size)
          (make-matrix (random-matrix-builder size))
          (error "Not a valid matrix size.")))))

;;; matrix.scm ends here.
