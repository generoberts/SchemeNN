;;; nn.scm --- Algorithms and defined-types for a neural network.

;; Commentary:
;; This file contains the defined-types for a layer and a model used in SchemeNN.
;; It also contains the so-call feedforward neural network and backpropagation
;; algorithms.
;; nn.scm depended on matrix.scm, activation.scm and cost.scm.

;; Code:

(define-library (schemeNN nn)
  (export make-layer make-model
          backpropagation
          feedforward model layers) ; debug
  (import (scheme base)
          (scheme cxr)
          (schemeNN matrix)
          (schemeNN activation)
          (schemeNN cost))
  (begin

    ;; A layer is essentially a collection of, mainly, matrix -- called weight matrix,
    ;; and procedure -- called activation function.
    ;; WEIGHT is a matrix which defined weights of each neuron in this layer.
    ;; ACTIVATION is a procedure that will be applied to the product of each neuron and its weight.
    ;; BIAS is a procedure that will apply to all elements of the matrix.
    ;; the next layer.
    (define-record-type <layer>
      (layer weight activation bias)
      layer?
      (weight weight-matrix set-weight-matrix!)
      (activation activation-func)
      (bias bias-func))

    ;; Makes it more convenient to define a layer because we don't have to use `random` everytime.
    ;; SIZE is a size of the weight matrix, which is a N-by-N matrix.
    ;; ACTIVATION is a name (symbol) of an activation function for this layer.
    ;; BIAS is a number that will be add to all the element of the output matrix.
    ;; This function returns a layer.
    (define (make-layer size activation bias)
      (layer (random-matrix size)
             (make-activation-function activation)
             (lambda (x) (+ bias x))))

    ;; Model is a list of layers
    (define-record-type <model>
      (model layers)
      model?
      (layers layers))

    ;; LAYER-SPECIFIERS is list of arguments that we use when creating a layers.
    ;; Each item in said list is for 1 layer.
    ;; In this convension, the order of the layer is the first item in the list is the
    ;; first layer, the second item is the second layer, and so on, and the last item
    ;; in the list is the last layer that will give a result of the model.
    ;; Return a model whose layers is order by the list descripted above.
    ;; Usage: (make-model '((2 tanh 1) (2 relu 1)))
    ;; TODO: Test
    (define (make-model layer-specifiers)
      (model (map (lambda (specifier) (map (lambda (weight-acti-bias)
                                        (make-layer (car weight-acti-bias)
                                                    (cadr weight-acti-bias)
                                                    (caddr weight-acti-bias)))
                                      specifier))
                  layer-specifiers)))

    ;; Doing the feedforward algorithm on INPT using the model MDL we defined.
    ;; MDL is a model.
    ;; INPT is a matrix (vector) of numbers.
    ;; Returs a list of matrixs, the result of applying model to the input.
    ;; Usage:
    ;; (define m (model (list (make-layer 2 'tanh 1))))
    ;; (define i (make-matrix '((0.5) '(0.3))))
    ;; (feedforward m i)
    ;; => (#<matrix-of-last-layer> #<matrix-of-second-to-last-layer> ... #<matrix-of-first-layer>)
    (define (feedforward mdl inpt)
      (define (calculate this-layer this-init)
        ;; BIAS(ACTIVATION(WEIGHT x INPUT))
        (matrix-apply (bias-func this-layer)
                      (matrix-apply (function (activation-func this-layer))
                                    (make-matrix (matrix-mul (weight-matrix this-layer)
                                                             (make-matrix this-init))))))

      ;; Applying each layer of the model to the input. The result looks like this:
      ;; LAYER-N(LAYER-N-1(... (LAYER-2(LAYER-1(INPUT)))))
      ;; that is, the result from each layer is passed to the next layer.
      ;; OUTPUT-COLLECTOR is a list contain all the outputs from each layer. It must be given as an empty list.
      ;; The order of OUTPUT-COLLECTOR is the output from the last layer is first item of the list.
      ;; TODO: test whether the order in OUTPUT-COLLECTOR correct or not.
      (define (ff lyres init output-collector)
        (let* ((this-layer (car lyres))
               (next-layer (cdr lyres))
               (output-this-layer (calculate this-layer init)))
          (if (null? next-layer)
              (cons output-this-layer output-collector)
              (ff next-layer
                  output-this-layer
                  (cons output-this-layer output-collector)))))

      (cond
       ((not (model? mdl))
        (error "Not a model."))
       ((not (matrix? inpt))
        (error "Input isn't a matrix"))
       (else
        (let ((l (layers mdl))
              (inpt-list (matrix-content inpt)))
          (ff (reverse l) inpt-list '())))))

    ;; For each input and each expected output, this function calculate the error of the
    ;; value(s) returned by the model, according to the cost function.
    ;; RSLT is  the list from `feedforward` function.
    ;; EXPECTED is a list of a row from expected matrix. 
    ;; CF is the cost function.
    ;; Returns a matrix of error value(s).
    (define (calculate-error rslt expected cf)
      (cond
       ((not (matrix? rslt))
        (error "RSLT isn't a matrix."))
       ((not (list? expected))
        (error "EXPECTED isn't a matrix."))
       ((not (cost-function? cf))
        (error "CF isn't a cost function."))
       (else
        ((cost-func cf) (matrix-content (car rslt)) expected))))

    ;; Calculating the dE/dx of the model and expected output.
    ;; RSLT is the first element of the list from `feedforward` function.
    ;; EXPECTED is a list of row from expected matrix.
    ;; CF is the cost function.
    ;; Returns a matrix contains the numerical result of derivatives of error function.
    (define (error-derivative rslt expected cf)
      (cond
       ((not (matrix? rslt))
        (error "RSLT isn't a matrix."))
       ((not (list? expected))
        (error "EXPECTED isn't a list."))
       (else
        ((gradient cf) (matrix-content (car rslt)) expected))))

    ;; Grouping the weight matrix of each layer in the model to a list.
    ;; MDL is the model.
    ;; Returns a list of matrix-content of each layer from the last layer
    ;; to the first layer.
    (define (collect-weight-matrix mdl)
      (reverse (map (lambda (l) (matrix-content (weight-matrix l))) (layers mdl))))

    ;; Grouping the derevatives of activation function of each layer in the model to a list.
    ;; MDL is the model.
    ;; Returns a list of types of derivatives of activation funtion from the function in
    ;; last layer to the function in the first layer.
    ;; TODO: test
    (define (collect-derivative mdl)
      (reverse (map (lambda (l) (derivative (activation-func l))) (layers mdl))))

    ;; Computing all the delta, starting from the last layer to the first.
    ;; dE/dx is a matrix of error value(s) obtained from `error-derivative`.
    ;; OUTS is a list of output matrix from each layer of the model.
    ;; WEIGHTS is a list of weight matrix from each layer of the model.
    ;; DERIVS is a list of derivative function from each layer of the model.
    ;; All of the lists has the corresponding data from the last layer to the first layer.
    ;; Returns a list of deltas from the last layer to the first layer.
    (define (deltas dE/dx outs weights derivs)
      (define (cal-delta dE/dx outs weights derivs dept collector)
        (let* ((index (- (length weights) dept))
               (big-F (matrix-apply (list-ref derivs index)
                                    (matrix-mul (list-ref outs index)
                                                (list-ref weights (+ 1 index)))))) ; TODO: check index out of range
          (cond
           ((= dept 1) ; the final layer, so we just return whatever we've collected and the last layer's delta
            (cons (hadamard-product (matrix-mul (list-ref weights (- 1 index))
                                                (caar collector))
                                    big-F)
                  collector))
           ((= index 0) ; the first layer, (final layer from feedforward)
            (cal-delta dE/dx
                       outs
                       weights
                       derivs
                       (- dept 1)
                       (cons (matrix-apply (lambda (ele) (* dE/dx ele))
                                           big-F)
                             collector)))
           (else
            (cal-delta dE/dx
                       outs
                       weights
                       derivs
                       (- dept 1)
                       (cons (hadamard-product (matrix-mul (list-ref weights (- 1 index))
                                                           (caar collector))
                                               big-F)
                             collector))))))
      ;; use `reverse` because we want to order it as the last layer to the first
      (map car (reverse (cal-delta dE/dx outs weights derivs (length outs) '()))))

    ;; MODEL is the model we're training
    ;; OUTS is the list of outputs from each of the layers
    ;; EXPECTED is the expected result the model should give
    ;; CF is the cost function to calculate a loss
    ;; LEARNING-RATE is, well, as the name imply
    ;; All of the list have their corresponding data from the last layer to the first
    ;; Returns a model but with a new list of weight matrixes
    (define (backpropagation model outs expected cf learning-rate)
      (define (cal-new-weights outs weights delta-list learning-rate collector)
        (if (null? outs)
            (reverse collector) ; reverse so the result is the first layer to the last layer
            (let ((new-weight (element-wise-matrix-operation -
                                                             (car weights)
                                                             (hadamard-product learning-rate
                                                                               (matrix-mul (car delta-list)
                                                                                           (car outs))))))
              (cal-new-weights (cdr outs)
                               (cdr weights)
                               (cdr delta-list)
                               learning-rate
                               (cons new-weight
                                     collector)))))
      (define (cal-backpropagation model outs weights delta-list learning-rate)
        (let ((new-weights (cal-new-weights outs weights delta-list learning-rate '())))
          (map (lambda (model-layer)
                 (map (lambda (ow nw) ; old weight and new weight
                        (set-weight-matrix! ow nw))
                      (reverse model-layer) (reverse new-weights))) ; since the layers the model is ordered by first to last
               (layers model))))

      (let* ((weight-list (collect-weight-matrix model))
             (delta-list (deltas (error-derivative (car outs) expected cf)
                                 outs
                                 weight-list
                                 (collect-derivative model))))
        (if (not (= (length outs) (length weight-list) (length delta-list)))
            (error "Invalide input range.")
            (cal-backpropagation model
                                 outs
                                 weight-list
                                 delta-list
                                 learning-rate))))))
;;; nn.scm ends here.
