(define (print x)
  (display x)
  (newline))
(define de 10)
(define o '(1 2 3 4 5))
(define w '(1 2 3 4 5))
(define d (list (lambda (x) (* x 1)) (lambda (x) (* x 1)) (lambda (x) (* x 1)) (lambda (x) (* x 1)) (lambda (x) (* x 1))))

(define m-apply
  (lambda (op m)
    name: 'm-apply
    (map (lambda (ele) (op ele)) m)))

(define m-mul
  (lambda (m1 m2)
    name: 'm-mul
    (map (lambda (e1 e2) (* e1 e2)) m1 m2)))

(define bigF
  (lambda (os ws ds index)
    name: 'bigF
    (m-apply (list-ref ds index)
             (list (* (list-ref ws index)
                      (list-ref os index))))))

(define cal
  (lambda (de os ws ds dept collector)
    name: 'cal
    (let* ((index (- (length ws) dept))
           (big-F (bigF os ws ds index)))
      (cond
       ((= dept 1) (cons
                    (m-mul
                     (list (* (car (reverse ws))
                              (caar collector)))
                     big-F)
                    collector)) ; we've reach the final layer
       ((= index 0) ; this the finallayer (since the layer is last to first)
        (cal de os ws ds (- dept 1) (cons
                                     (map (lambda (ele) (* de ele))
                                          big-F)
                                     collector)))
       (else
        (let ((pd (car collector)))
          (cal de os ws ds (- dept 1) (cons
                                       (m-mul
                                        
                                        (list (* (list-ref ws (+ 1 index))
                                                 (car pd)))
                                        big-F)
                                       collector))))))))

(define (main)
  (trace cal)
  (trace bigF)
  (trace m-mul)
  (trace m-apply)
  (print (cal de o w d (length w) '())))
