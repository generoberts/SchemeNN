;;; activation.scm --- Activation functions and their derivatives.

;; Commentary:
;; Activation function is what will be applied to the result of multiplication
;; between each neuron's output and its weight matrix. And their
;; derivatives are used to calculate how much we should adjust each
;; weight for each neuron.
;; This file contains several of popular activation functions but there're
;; more of them that aren't implmented here.
;; TODO: test all these activation functinos.

;; Code:

(define-library (schemeNN activation)
  (export make-activation-function
          activation-function?
          function derivative
          identity didentity
          binary-step dbinary-step
          logistic dlogistic
          tanh dtanh
          relu drelu
          leaky-relu dleaky-relu
          softplus dsoftplus)
  (import (scheme base)
          (scheme inexact))
  (begin

    ;; Activation functions are taken their definition and derivatives from
    ;; https://en.wikipedia.org/wiki/Activation_function
    ;; ACTIVATION-TYPE of activation function is its name. Once created, activation function
    ;; can't change its type.
    ;; DERIVATIVE is named as its corresponded function but prefix
    ;; with a 'd'.
    (define-record-type <activation-function>
      (activation-function function derivative)
      activation-function?
      (function function)
      (derivative derivative))

    ;; Takes a NAME which must be symbol of a defined activation function
    ;; Returns an activation function.
    ;; Usage: (make-activation-function 'relu) ; => #<activation-function>
    (define (make-activation-function name)
      (let ((func-deri (case name
                         ((identity) (cons identity didentity))
                         ((binary-step) (cons binary-step dbinary-step))
                         ((logistic) (cons logistic dlogistic))
                         ((tanh) (cons tanh dtanh))
                         ((relu) (cons relu drelu))
                         ((leaky-relu) (cons leaky-relu dleaky-relu))
                         ((softplus) (cons softplus dsoftplus))
                         (else (error "Unknow activation function")))))
        (activation-function (car func-deri) (cdr func-deri))))

    ;; TODO: not done defining this yet.
    ;; Takes a vector (or a list) as input.
    (define (softmax vect)
      (let ((summation (apply + (map exp vect))))
        (map (lambda (val) (/ (exp val) summation)) vect)))
    ;; (define (dsoftmax vect))

    ;; TODO: not done defining this yet.
    ;; Also takes a vector (or a list) as input
    (define (maxout vect)
      (apply max vect))
    ;; (defnie (dmaxout vect))

    ;; ------------------------------------------------------------
    ;; All functions below here takes only 1 number as its argument.

    (define (identity x)
      x)
    (define (didentity x)
      1)

    (define (binary-step x)
      (if (>= x 0)
          1
          0))
    (define (dbinary-step x)
      (if (= x 0)
          1 ; here we hardcode it, since Wikipedia doesn't specify the value.
          0))

    (define (logistic x)
      (/ 1 (+ 1 (exp (- x)))))
    (define (dlogistic x)
      (* (logistic x) (- 1 (logistic x))))

    (define (tanh x)
      (define (e^x+-e^-x arg plus?)
        ((if plus? + -) (exp arg) (exp (- x))))
      (/ (e^x+-e^-x x #f)
         (e^x+-e^-x x #t)))
    (define (dtanh x)
      (- 1 (expt (tanh x) 2)))

    (define (relu x)
      (if (> x 0)
          x
          0))
    (define (drelu x)
      (if (> x 0)
          1
          0))

    (define (leaky-relu x)
      (if (>= x 0)
          x
          (* 0.01 x)))
    (define (dleaky-relu x)
      (if (>= x 0)
          1
          0.01))

    (define (softplus x)
      (log (+ 1 (exp x))))
    (define (dsoftplus x)
      (/ 1 (+ 1 (exp (- x)))))))

;;; activation.scm ends here.
