;;; cost.scm --- Cost functions and their gradients.

;; Commentary:
;; Similar to activation.scm, a cost function is a collection of its name (type),
;; procedure that computes the cost between an input and what it expects,
;; along with its gradient which will be used to update the weight matrix.
;; Gradient has a name like its cost function but prefix with 'g'.
;;_TODO:_There're_still_more_cost_functions_to_implement.

;; Code:

(define-library (schemeNN cost)
  (export make-cost-function
          cost-function?
          cost-func gradient
          mse gmse
          cross-entropy gcross-entropy)
  (import (scheme base)
          (scheme inexact)
          (schemeNN matrix))
  (begin

    (define-record-type <cost-function>
      (cost-function cost-func gradient)
      cost-function?
      (cost-func cost-func)
      (gradient gradient))

    ;; The NAME must be a symbol of a cost function.
    ;; Returns a cost function.
    ;; Usage: (make-cost-function 'mse)
    (define (make-cost-function name)
      (let ((func-deri (case name
                         ((mse) (cons mse gmse))
                         ((cross-entropy) (cons cross-entropy gcross-entropy))
                         (else (error "Unknow cost function")))))
        (cost-function (car func-deri)
                       (cdr func-deri))))

    ;; RESULT is a matrix, which  is given from the feedforward function.
    ;; EXPECTED is a matrx, which is given from the training data.
    ;; Returns a matrix represents the cost between RESULT and EXPECTED.
    (define (mse result expected)
      (cond
       ((not (matrix? result))
        (error "RESULT isn't a matrix."))
       ((not (matrix? expected))
        (error "EXPECTED isn't a matrix."))
       (else
        (make-matrix (map (lambda (final) (* 0.5 final))
                          (map  +
                                (map (lambda (x) (* (car x) (car x)))
                                     (map (lambda (r e) (map - r e))
                                          (matrix-content result)
                                          (matrix-content expected)))))))))
    (define (gmse result expected)
      (cond
       ((not (matrix? result))
        (error "RESULT isn't a matrix."))
       ((not (matrix? expected))
        (error "EXPECTED isn't a matrix."))
       (else
        (make-matrix (map - (matrix-content result)  (matrix-content expected))))))

    ;; RESULT is a matrix, which  is given from the feedforward function.
    ;; EXPECTED is a matrx, which is given from the training data.
    ;; Returns a matrix represents the cost between RESULT and EXPECTED.
    (define (cross-entropy result expected)
      (cond
       ((not (matrix? result))
        (error "RESULT isn't a matrix."))
       ((not (matrix? expected))
        (error "EXPECTED isn't a matrix."))
       (else
        (- (make-matrix (apply + (map (lambda (r e) (+ (* e (log r)) (* (- 1 e) (log (- 1 r)))))
                                      (matrix-content result)
                                      (matrix-content expected))))))))
    (define (gcross-entropy result expected)
      (cond
       ((not (matrix? result))
        (error "RESULT isn't a matrix."))
       ((not (matrix? expected))
        (error "EXPECTED isn't a matrix."))
       (else
        (make-matrix (map (lambda (r e) (/ (- r e) (* (- 1 r) r)))
                          (matrix-content result)
                          (matrix-content expected))))))))

;;; cost.scm ends here.
