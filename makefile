all: random.class util.class matrix.class activation.class cost.class nn.class
	kawa --main -C main.scm

random.class:
	kawa -C random.scm

util.class:
	kawa -C util.scm

matrix.class:
	kawa -C matrix.scm

activation.class:
	kawa -C activation.scm

cost.class:
	kawa -C cost.scm

nn.class:
	kawa -C nn.scm

clean:
	rm ./schemeNN/*.class && rm main.class
