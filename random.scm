;;; random.scm -- Library for random number generator.

;; Commentary:
;; This file uses Kawa Scheme specific functions to provide common random function
;; used in SchemeNN. You may use your own implementation's functions to do so, given
;; that it has functions to do the following:
;; 1. Randomly choese an integer from 0 to any given integer
;; 2. Randomly choose an real (inexact in Scheme terminology) numbers between 0 and 1.

;; Code:
(define-library (schemeNN random)
  (export random rrandom)
  (import (scheme base)
          (only (kawa base) make))
  (begin
    (define rand ::java.util.Random (make java.util.Random))

    ;; Randoms an integer 0 to (- n 1)
    (define (random n)
      (rand:nextInt n))


    ;; Randomly chooses an uniformly distributed random inexact number between 0 and 1.
    (define (rrandom)
      (rand:nextGaussian))))

;; random.scm ends here.
